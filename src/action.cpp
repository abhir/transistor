#include "../include/action.h"

//////////////////////////////////////////////////////////////////////
//	class Action_List
//////////////////////////////////////////////////////////////////////

//	Action_List::update : update the current list
void Action_List::update (float dt)
{
	iter = actions.begin();		// reset the iterator to beginnning of the list
	for (Actions_iterator& action = iter; action != actions.end(); ++action) {
		(*action)->update(dt);	// update the Action
		if ((*action)->is_blocking)	// stop processing if Action is blocking
			break;
		if((*action)->is_finished) {
			(*action)->on_end();	// Action is finished now, invoke the on_end function and remove it from list
			this->remove(action);
		}
	}
}

//	Action_List::push_front : insert Action at the beginning of the list
void Action_List::push_front (Action* action) {
	iter = actions.insert(actions.begin(), action);
	action->owner_list = this;
	action->on_start();	// Action added to list, invoke start() function
}

//	Action_List::push_back : append Action to the end of the list
void Action_List::push_back (Action* action) {
	actions.push_back(action);
	iter = actions.end();
	--iter;
	action->owner_list = this;
	action->on_start();
}

//	Action_List::insert_before : insert Action before the iterator (iterator is from Action_list::current_iterator())
void Action_List::insert_before (const Actions_iterator& iterator, Action* action) {
	iter = actions.insert(iterator, action);
	action->owner_list = this;
	action->on_start();
}

//	Action_List::insert_after : insert Action after the iterator (iterator is from Action_list::current_iterator())
void Action_List::insert_after (Actions_iterator iterator, Action* action) {
	iter = actions.insert(++iterator, action);
	action->owner_list = this;
	action->on_start();
}

//	Action_List::remove : remove the Action pointed to by the iterator
void Action_List::remove (Actions_iterator iterator) {
	actions.erase(iterator);
}
