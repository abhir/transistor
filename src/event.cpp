#include "../include/event.h"

//////////////////////////////////////////////////////////////////////
//	class Event_Manager
//////////////////////////////////////////////////////////////////////

//	Event_Manager : sets the defaults
Event_Manager::Event_Manager() {
	keyboard_state = (Uint8*)SDL_GetKeyboardState(nullptr);
}	
	
//	Event_Manager : free up the resources
Event_Manager::~Event_Manager() {
	// TODO: add termination code here
}

//	Event_Manager::add_event_handler : adds an handler for events
void Event_Manager::add_event_handler(Event_Handler& handler) {
	event_handlers.push_back(&handler);
}

//	Event_Manager::add_state_handler : adds an handler for states
void Event_Manager::add_state_handler(Event_Handler& handler) {
	state_handlers.push_back(&handler);
}

//	Event_Manager::remove_event_handler : remove the handler from list
void Event_Manager::remove_event_handler(Event_Handler& handler) {
	for (vector<Event_Handler*>::iterator iter = event_handlers.begin(); iter != event_handlers.end(); ++iter)
		if (*iter == &handler) {	// TODO : do something better than checking addresses
			event_handlers.erase(iter);
			break;
		}
}

//	Event_Manager::remove_state_handler : remove the handler from list
void Event_Manager::remove_state_handler(Event_Handler& handler) {
	for (vector<Event_Handler*>::iterator iter = state_handlers.begin(); iter != state_handlers.end(); ++iter)
		if (*iter == &handler) {	// TODO : do something better than checking addresses 
			state_handlers.erase(iter);
			break;
		}
}

//	Event_Manager::poll_handle: poll and handle events
void Event_Manager::poll_handle() {
	SDL_PumpEvents();	// populate the keyboard_state array
	for (vector <Event_Handler*>::iterator iter = state_handlers.begin(); iter != state_handlers.end(); ++iter)
		(*iter)->handle_state(keyboard_state);	// sent to the handlers

	while (SDL_PollEvent(&event)) {		// get an event
		for (vector <Event_Handler*>::iterator iter = event_handlers.begin(); iter != event_handlers.end(); ++iter) {	// iterate over the handlers
			if ((*iter)->event_type() & event.type)	// match the types
				(*iter)->handle_event(event);		// pass to handler
		}
	}
}
