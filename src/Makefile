# g++ or clang++
CXX = g++
SDL_INCLUDE = $(shell sdl2-config --cflags)
SDL_LIB = $(shell sdl2-config --libs) -lSDL2_image -lSDL2_ttf
CXXFLAGS = -Wall -std=c++11
LDFLAGS = $(SDL_LIB)

EXE = sdl_output
SRC = common.cpp event.cpp action.cpp graphics.cpp physics.cpp base_game.cpp factory.cpp main.cpp
OBJ = $(SRC:.cpp=.o)

# ========== TARGETS ==========
# default
all: $(EXE)

# debug
debug: CXXFLAGS += -g
debug: clean
debug: $(EXE)

# profile
profile: CXXFLAGS += -pg
profile: LDFLAGS += -pg
profile: clean
profile: $(EXE)

# EXE
$(EXE): $(OBJ)
	$(CXX) $^ $(LDFLAGS) -o $@

%.o:	%.cpp
	$(CXX) -c $(CXXFLAGS) $(SDL_INCLUDE) $< -o $@

.PHONY: clean all help depend
# depend: create dependencies
depend: .depend

.depend: $(SRC)
	$(CXX) $(CXXFLAGS) -MM $^ > ./.depend

# clean
clean:
	@echo "Cleaning up"
	$(RM) $(OBJ) $(EXE) .depend

# help
help:
	@echo ""
	@echo "make         - builds ${EXE}"
	@echo "make profile - builds ${EXE} with profile (-pg) flag"
	@echo "make debug   - builds ${EXE} with debug (-g) flag"
	@echo "make clean   - deletes prior build"
	@echo "make help    - prints this help"

-include .depend
