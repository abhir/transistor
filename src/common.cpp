#include "../include/common.h"

//	Initialize the ID generator counter to 0
ID ID_gen::next_ID = 0;

//////////////////////////////////////////////////////////////////////
//	class Circle
//////////////////////////////////////////////////////////////////////

//	Circle::calculate_points : calculate the points of circle using mid point circle algorithm
void Circle::calculate_points(const Point& centre, unsigned int radius) {
	Point p(radius, 0);
	int radius_error = 1 - p.x;

	points.clear();		// clear old points
	while (p.x >= p.y) {
		//	add the 8 symmetric points
		points.push_back(Point(p.x + centre.x, p.y + centre.y));
		points.push_back(Point(p.y + centre.x, p.x + centre.y));
		points.push_back(Point(-p.x + centre.x, p.y + centre.y));
		points.push_back(Point(-p.y + centre.x, p.x + centre.y));
		points.push_back(Point(-p.x + centre.x, -p.y + centre.y));
		points.push_back(Point(-p.y + centre.x, -p.x + centre.y));
		points.push_back(Point(p.x + centre.x, -p.y + centre.y));
		points.push_back(Point(p.y + centre.x, -p.x + centre.y));

		p.y++;
		if (radius_error < 0) {
			radius_error += (p.y * 2) + 1;
		}
		else {
			p.x--;
			radius_error += ((p.y - p.x + 1) * 2);
		}
	}
}

//////////////////////////////////////////////////////////////////////
//	class Vec2
//////////////////////////////////////////////////////////////////////

//	Vec2 operators
Vec2 operator - (const Vec2& a, const Vec2& b) {
	return Vec2 (a.x - b.x, a.y - b.y);
}
Vec2 operator - (const Vec2& a, float c) {
	return Vec2 (a.x - c, a.y - c);
}
Vec2 operator - (const Vec2& a) {
	return Vec2 (-a.x, -a.y);
}
Vec2 operator -= (Vec2& a, const Vec2& b) {
	return a = a-b;
}

Vec2 operator + (const Vec2& a, const Vec2& b) {
	return Vec2 (a.x + b.x, a.y + b.y);
}

Vec2 operator + (const Vec2& a, float c) {
	return Vec2 (a.x + c, a.y + c);
}

Vec2 operator += (Vec2& a, const Vec2& b) {
	return a = a+b;
}

//	scalar product
Vec2 operator * (const Vec2& a, float f) {
	return Vec2 (a.x * f, a.y * f);
}

//	scalar division
Vec2 operator / (const Vec2& a, float f) {
	return Vec2 (a.x / f, a.y / f);
}

bool operator == (const Vec2& a, const Vec2& b) {
	return a.x == b.x && a.y == b.y;
}
bool operator < (const Vec2& a, const Vec2& b) {
	return a.x < b.x && a.y < b.y;
}

//	dot_product between two vectors
float dot_product (const Vec2& a, const Vec2& b) {
	return (a.x * b.x + a.y * b.y);
}

//	absolute value of individual components of the vector
Vec2 abs (const Vec2& a) {
	return Vec2(abs(a.x), abs(a.y));
}
