#!/usr/bin/python3.3
# plots.py
# plot the velocity and friction curves using 'debug' data
# capture debug using: $ ./sdl_output 2> debug

import re
import matplotlib.pyplot as plt

def plot_2(fname, regex, title):
	fin = open(fname)
	ll1 = []
	sum1 = 0
	ll2 = []
	sum2 = 0
	for i in fin:
		if re.match(regex, i) is None:
			continue
		s, x, y = i.split()
		ll1.append(float(x))
		sum1 += float(x)
		ll2.append(float(y))
		sum2 += float(y)

	if len(ll1) == 0 and len(ll2) == 0:
		print ('No data found')
		return

	plt.subplot(211)
	plt.plot(ll1)
	plt.title(title)

	plt.subplot(212)
	plt.plot(ll2)
	plt.title(title)

	plt.show()
	print (title, 'graph plotted')

def plot_1(fname, regex, title):
	fin = open(fname)
	ll = []
	sum = 0
	for i in fin:
		if re.match(regex, i) is None:
			continue
		s, x = i.split()
		ll.append(float(x))
		sum += float(x)

	if len(ll) == 0:
		print ('No data found')
		return

	plt.plot(ll)
	plt.title(title)

	plt.show()
	print (title, 'graph plotted')

if __name__ == "__main__":
	import sys
	import getopt

	args = getopt.getopt(sys.argv[1:], 't:r:k:f:h')
	fname = 'debug'
	k = 1
	r = r'.'
	t = 'Value'
	for i in args[0]:
		if i[0] == '-t':
			t = i[1]
		elif i[0] == '-r':
			r = i[1]
		elif i[0] == '-k':
			k = int(i[1])
		elif i[0] == '-f':
			fname = i[1]
		else:
			print ('Usage: [-t <title>] [-r <regex>] -k <items(1,2)> [-f <filename>(default: debug)]')
			sys.exit (1)
	if k == 1:
		plot_1 (fname, r, t)
	elif k == 2:
		plot_2 (fname, r, t)
