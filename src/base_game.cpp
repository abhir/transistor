#include "../include/base_game.h"

//	Circle_Texture::Circle_Texture
Circle_Texture::Circle_Texture (unsigned radius, Color color) {
	c.calculate_points(Point(radius, radius), radius);

	create_blank(radius * 2 + 1, radius * 2 + 1, SDL_TEXTUREACCESS_TARGET);
	Renderer::set_render_target(this->texture());
	Renderer::set_draw_color(color);

	if (SDL_RenderDrawPoints(Renderer::renderer(), (SDL_Point*)c.data(), c.size()) != 0)
		cerr<<"ERROR: "<<__func__<<" SDL_RenderDrawPoints failed!\nSDL Error: "<<SDL_GetError()<<'\n';

	Renderer::reset_render_target();
}

//	Rectangle_Texture::Rectangle_Texture
Rectangle_Texture::Rectangle_Texture(int w, int h) {
	SDL_Rect rect = { 0, 0, w, h };
	create_blank(w, h, SDL_TEXTUREACCESS_TARGET);

	Renderer::set_render_target(this->texture());
	Renderer::set_draw_color(Color::RED);
	if (SDL_RenderFillRect(Renderer::renderer(), &rect) != 0)
		cerr<<"ERROR: "<<__func__<<" SDL_RenderDrawRect failed!\nSDL Error: "<<SDL_GetError()<<'\n';
	Renderer::reset_render_target();
}

//	Sphere::Sphere
Sphere::Sphere (Texture_Stream* texture_stream, Vec2 top_left_position, Vec2 velocity, float inv_mass) {
	this->curr_frame = 0;
	this->tex_stream = texture_stream;
	Texture* ctex = tex_stream->get_texture (curr_frame);

	body.pos = top_left_position + Vec2(ctex->width()/2, ctex->width()/2);
	body.velocity = velocity;
	body.gravity_scale = GRAVITY_DEFAULT / 20;
	body.inv_mass = inv_mass;
	body.restitution = 1.0f;
	body.static_friction = 0.1f;
	body.dynamic_friction = 0.0f;

	body.shape = &shape;
	shape.body = &body;
	shape.rad = ctex->width()/2;
	shape.centre = body.pos;
}

//	Sphere::update
void Sphere::update (float dt) {
	const float DIST = 50.0f;
	static float tick = DIST;
	tick -= ((body.velocity) * dt).magnitude();

	if (tick <= 0.0f) {
		curr_frame++;
		tick = DIST;
	}
}

//	Sphere::render
void Sphere::render(const Camera& camera) {
	Rect tmp(Point(body.pos.x-shape.rad, body.pos.y-shape.rad), shape.rad*2, shape.rad*2);
	if (SDL_HasIntersection ((SDL_Rect*)&camera, (SDL_Rect*)&tmp))
		tex_stream->get_texture (curr_frame)->render(Point(body.pos.x - shape.rad - camera.x, body.pos.y - shape.rad - camera.y));
}

//	Guided_Sphere::Guided_Sphere
Guided_Sphere::Guided_Sphere (Texture* texture, Vec2 top_left_position, Vec2 velocity, float inv_mass) {
	tex = texture;

	gs_body.pos = top_left_position + Vec2(tex->width()/2, tex->width()/2);
	gs_body.velocity = velocity;
	gs_body.gravity_scale = 0;
	gs_body.inv_mass = inv_mass;
	gs_body.restitution = 1.0f;
	gs_body.static_friction = 0.1f;
	gs_body.dynamic_friction = 0.0f;

	gs_body.shape = this;
	this->body = &gs_body;
	this->rad = tex->width()/2;
}

//	Guided_Sphere::on_start
void Guided_Sphere::on_start () {
	if (!cmd.size()) {
		cerr<<"WARNING: Guided_Sphere::"<<__func__<<": cmd not set!\n";
		owner_list->remove(owner_list->current_iterator());
		curr_cmd = -1;
	}
	curr_cmd = 0;

}

//	Guided_Sphere::update
void Guided_Sphere::update (float dt) {
	const Vec2 VEL1 = Vec2 (0.0f, 5.0f);
	const Vec2 VEL2 = Vec2 (5.0f, 0.0f);

	if (curr_cmd == -1)
		return;
	else {
		curr_cmd = (curr_cmd+1)%cmd.size();
		switch (cmd[curr_cmd]) {
			case 'x':
				return;
			case 'w':
				gs_body.velocity -= VEL1;
				break;
			case 's':
				gs_body.velocity += VEL1;
				break;
			case 'a':
				gs_body.velocity -= VEL2;
				break;
			case 'd':
				gs_body.velocity += VEL2;
				break;
		}
	}
}

//	Guided_Sphere::render
void Guided_Sphere::render (const Camera& camera) {
	Rect r(Point(gs_body.pos.x-this->rad, gs_body.pos.y-this->rad), this->rad*2, this->rad*2);
	if (SDL_HasIntersection((SDL_Rect*)&camera, (SDL_Rect*)&r))
		tex->render (Point(gs_body.pos.x - this->rad - camera.x, gs_body.pos.y - this->rad - camera.y));
}

//	Slab::Slab
Slab::Slab (Rectangle_Texture* texture, Vec2 top_left_position, Vec2 velocity, float inv_mass) {
	rtex = texture;

	slab_body.pos = top_left_position + Vec2(rtex->width()/2, rtex->height()/2);
	slab_body.velocity = velocity;
	slab_body.gravity_scale = 0.0f;
	slab_body.inv_mass = inv_mass;
	slab_body.restitution = 0.8f;
	slab_body.static_friction = 0.1f;
	slab_body.dynamic_friction = 0.009f;

	slab_body.shape = this;
	this->body = &slab_body;
	this->min = top_left_position;
	this->max = top_left_position + Vec2(rtex->width(), rtex->height());
}

//	Slab::render
void Slab::render(const Camera& camera) {
	Rect tmp(Point(slab_body.pos.x-rtex->width()/2, slab_body.pos.y-rtex->height()/2), rtex->width(), rtex->height());
	if (SDL_HasIntersection ((SDL_Rect*)&camera, (SDL_Rect*)&tmp))
		rtex->render(Point(slab_body.pos.x - rtex->width()/2 - camera.x, slab_body.pos.y - rtex->height()/2 - camera.y));
}

//	Player_Box::Player_Box
Player_Box::Player_Box (Texture_Stream* texture_stream, Vec2 top_left_position, Vec2 velocity, float inv_mass) { 
	this->curr_frame = 0;

	ptex = texture_stream;
	Texture* t = ptex->get_texture (curr_frame);

	player_body.gravity_scale = GRAVITY_DEFAULT;
	player_body.restitution = 0.01f;
	player_body.inv_mass = inv_mass;

	player_body.static_friction = 0.9f;
	player_body.dynamic_friction = 0.9f;

	player_body.pos = top_left_position + Vec2(t->width()/2, t->height()/2);
	player_body.velocity = velocity;

	player_body.shape = this;
	this->body = &player_body;
	this->min = top_left_position;
	this->max = top_left_position + Vec2(t->width(), t->height());
}

//	Player_Box::render
void Player_Box::render(const Camera& camera) {
	Texture* t = ptex->get_texture(curr_frame);
	t->render(Point(player_body.pos.x - t->width()/2 - camera.x, player_body.pos.y - t->height()/2 - camera.y));
}

//	Player::Player
Player::Player (Texture_Stream* texture_stream, Vec2 top_left_position, Vec2 velociy, float inv_mass) :
       Player_Box (texture_stream, top_left_position, velociy, inv_mass),
       Event_Handler (SDL_KEYDOWN | SDL_KEYUP),
       can_jump (false)
{	
	friction1 = 0.9f;
	friction2 = 0.01f;

	JUMP_KEY = SDL_SCANCODE_UP;
	JUMP_BOOST_KEY = SDL_SCANCODE_W;
	TURN_LEFT_KEY = SDL_SCANCODE_LEFT;
	TURN_RIGHT_KEY = SDL_SCANCODE_RIGHT;
}

//	Player::update
void Player::update (float dt) {
	const float TIMEOUT = 0.8f;
	static float tick = TIMEOUT;
	can_jump = false;

	if (abs(player_body.velocity.x) <= VELOCITY_EPSILON * 2) {
		curr_frame = curr_frame > 3 ? 4 : 0;
		tick = TIMEOUT;
	}
	else if (player_body.velocity.x > 0) {
		tick -= dt;
		curr_frame = curr_frame == 0 ? 1 : curr_frame;
		if (tick < 0.0f) {
			curr_frame = (curr_frame)%3+1;
			tick = TIMEOUT;
		}
	}
	else if (player_body.velocity.x < 0) {
		tick -= dt;
		curr_frame = curr_frame == 0 ? 3 : curr_frame;
		if (tick < 0.0f) {
			curr_frame = (curr_frame)%3+5;
			tick = TIMEOUT;
		}
	}
}

//	Player::pre_collision
void Player::pre_collision (const Manifold& m) {
	const Vec2 HORIZ(1.0f, 0.0f);
	if ((dot_product (HORIZ, abs(m.normal))) > 0.5f) {
		player_body.static_friction = friction2;
		player_body.dynamic_friction = friction2;
	}
}

//	Player::post_collision
void Player::post_collision (const Manifold& m) {
	const Vec2 JUMP_VEC(0.0f, -1.0f);
	if (dot_product (JUMP_VEC, ((m.a == &player_body) ? -m.normal : m.normal)) > 0.5f)
		can_jump = true;
	player_body.static_friction = friction1;
	player_body.dynamic_friction = friction1;
}

//	Player::handle_state
void Player::handle_state (const Uint8* key_state) {
	const Vec2 VEL2(30.0f, 0.0f);
	Vec2 F1(0, 30000.0f);


	if (key_state[JUMP_KEY]) {
		if (can_jump) {
			if (key_state[JUMP_BOOST_KEY])
				F1 += Vec2 (0, 25000.0f);
			player_body.force -= F1;
			can_jump = false;
		}
	}
	/*
	if (key_state[SDL_SCANCODE_DOWN]) {
		if (abs(body.velocity.y) > VELOCITY_EPSILON * 2) {
			body.force += F1;
		}
	}
	*/
	if (key_state[TURN_LEFT_KEY]) {
		if (-(VEL2.x) < player_body.velocity.x) {
			player_body.velocity.x = -VEL2.x;
		}
	}
	if (key_state[TURN_RIGHT_KEY]) {
		if (player_body.velocity.x < (VEL2.x)) {
			player_body.velocity.x = VEL2.x;
		}
	}
}
