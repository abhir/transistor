#include "../include/graphics.h"

//////////////////////////////////////////////////////////////////////
//	Color
//////////////////////////////////////////////////////////////////////

// some basic Colors
Color Color::BLACK = Color(0, 0, 0);
Color Color::WHITE = Color(0xFF, 0xFF, 0xFF);
Color Color::RED = Color(0xFF, 0, 0);
Color Color::GREEN = Color(0, 0xFF, 0);
Color Color::BLUE = Color(0, 0, 0xFF);
Color Color::TRANSPARENT = Color(0, 0, 0, 0);

//////////////////////////////////////////////////////////////////////
//	class Window
//////////////////////////////////////////////////////////////////////

//	Window : default constructor for 'Window'. Just sets the different params to default values
Window::Window()
	: Event_Handler(SDL_WINDOWEVENT | SDL_QUIT)
{
	*this = Window("Unnamed Window");
}

//	Window : sets the title to string 's'
Window::Window(const string& s, int w, int h) :
	Event_Handler(SDL_WINDOWEVENT),
	win(nullptr),
	whidden(true),		// 'Window' is hidden
	wquit(false)
{
	wrect.x = SDL_WINDOWPOS_UNDEFINED;
	wrect.y = SDL_WINDOWPOS_UNDEFINED;
	wrect.w = w;
	wrect.h = h;
	title(s);	// sets the title of current window to 's' (defaults to 'Unnamed Window')
}

Window::~Window() {
	free();		// delete the SDL_Window
}

//	Window::init : SDL_Window is created and position and size is set
void Window::init() {
	win = SDL_CreateWindow(wtitle.c_str(), wrect.x, wrect.y, wrect.w, wrect.h, WINDOW_FLAGS);
	if (win == nullptr)
		throw Bad_Window(__func__ + string(": SDL_CreateWindow failed.\nSDL Error: ") + SDL_GetError());
	whidden = false;	// Window is visible now
	SDL_GetWindowPosition(win, &wrect.x, &wrect.y);
	SDL_GetWindowSize(win, &wrect.w, &wrect.h);

	SDL_Log ("Window %d created at ( %d, %d ) with dimensions ( %d, %d )\n", SDL_GetWindowID (win), wrect.x, wrect.y, wrect.w, wrect.h);
}

//	Window::free : destroy the SDL_Window and reset the params
void Window::free() {
	if (win != nullptr) {
		SDL_DestroyWindow(win);
	}

	wrect.w = wrect.h = 0;
	title("");	// reset title
	whidden = true;
}

//	Window::pos : set the Window position to Point p (global coordinates are used)
void Window::pos(Point p) {
	wrect.x = p.x >= 0 ? p.x : SDL_WINDOWPOS_CENTERED;	// set to CENTERED if -ve
	wrect.y = p.y >= 0 ? p.y : SDL_WINDOWPOS_CENTERED;

	SDL_SetWindowPosition(win, wrect.x, wrect.y);	// update position
}

//	Window::resize : change the Window's width and height
void Window::resize(int w, int h) {
	wrect.w = w > 0 ? w : DEFAULT_WIDTH;	// do not accept -ve width
	wrect.h = h > 0 ? h : DEFAULT_HEIGHT;	// do not accept -ve height

	SDL_SetWindowSize(win, wrect.w, wrect.h);	// upadate size
}

const char* window_event_to_string (int i) {
	static map <int, string> mapper;
	if (mapper.size() == 0) {
		mapper[SDL_WINDOWEVENT_NONE] = "SDL_WINDOWEVENT_NONE";
		mapper[SDL_WINDOWEVENT_SHOWN] = "SDL_WINDOWEVENT_SHOWN";
		mapper[SDL_WINDOWEVENT_HIDDEN] = "SDL_WINDOWEVENT_HIDDEN";
		mapper[SDL_WINDOWEVENT_EXPOSED] = "SDL_WINDOWEVENT_EXPOSED";
		mapper[SDL_WINDOWEVENT_MOVED] = "SDL_WINDOWEVENT_MOVED";
		mapper[SDL_WINDOWEVENT_RESIZED] = "SDL_WINDOWEVENT_RESIZED";
		mapper[SDL_WINDOWEVENT_SIZE_CHANGED] = "SDL_WINDOWEVENT_SIZE_CHANGED";
		mapper[SDL_WINDOWEVENT_MINIMIZED] = "SDL_WINDOWEVENT_MINIMIZED";
		mapper[SDL_WINDOWEVENT_MAXIMIZED] = "SDL_WINDOWEVENT_MAXIMIZED";
		mapper[SDL_WINDOWEVENT_RESTORED] = "SDL_WINDOWEVENT_RESTORED";
		mapper[SDL_WINDOWEVENT_ENTER] = "SDL_WINDOWEVENT_ENTER";
		mapper[SDL_WINDOWEVENT_LEAVE] = "SDL_WINDOWEVENT_LEAVE";
		mapper[SDL_WINDOWEVENT_FOCUS_GAINED] = "SDL_WINDOWEVENT_FOCUS_GAINED";
		mapper[SDL_WINDOWEVENT_FOCUS_LOST] = "SDL_WINDOWEVENT_FOCUS_LOST";
		mapper[SDL_WINDOWEVENT_CLOSE] = "SDL_WINDOWEVENT_CLOSE";
	}

	return mapper[i].c_str();
}

//	Window::handle_event : handle the event supplied
void Window::handle_event(const SDL_Event& event) {
	if (event.type == SDL_QUIT)
		wquit = true;

	switch (event.window.event) {
	case SDL_WINDOWEVENT_SHOWN:		// window is now visible
		whidden = false;
		break;
	case SDL_WINDOWEVENT_HIDDEN:		// window is now hidden
		whidden = true;
		break;
	case SDL_WINDOWEVENT_CLOSE:		// user pressed 'x' button
		wquit = true;
		break;
	default:
		SDL_Log("Window %d got event %s", event.window.windowID, window_event_to_string(event.window.event));
	}
}

//////////////////////////////////////////////////////////////////////
//	class Renderer
//////////////////////////////////////////////////////////////////////

//	Initialization of static data members
SDL_Renderer* Renderer::ren = nullptr;
Color Renderer::rcolor = Color::BLACK;

//	Renderer : create the SDL_Renderer
void Renderer::create_renderer(Window& win)
{
	ren = SDL_CreateRenderer(win.window(), -1, RENDERER_FLAGS);	// -1 for specifying default rendering driver
	if (ren == nullptr)
		throw Exception(__func__ + string(": SDL_CreateRenderer failed.\nSDL Error: ") + SDL_GetError());
	if (SDL_GetRenderDrawColor(ren, &rcolor.color()->r, &rcolor.color()->g, &rcolor.color()->b, &rcolor.color()->a) != 0) {
		cerr<<"WARNING: "<<__func__<<": SDL_GetRenderDrawColor failed!\nSDL Error: "<<SDL_GetError()<<'\n';
		rcolor = Color::BLACK;
	}
}

//	Renderer:set_rendering_resolution : set device independent resolution for rendering
void Renderer::set_rendering_resolution (int w, int h) {
	SDL_RenderSetLogicalSize (Renderer::renderer(), w, h);
}

//	Renderer::free : destroy the SDL_Renderer
void Renderer::free() {
	if (ren != nullptr)
		SDL_DestroyRenderer(ren);
}

//	Renderer::render_screen : render the current scene
//	NOTE : Use this function after all calls to 'render' have been made. Actual drawing takes place here.
void Renderer::render_screen() {
	SDL_RenderPresent(ren);
}

//	Renderer::clear_screen : clear the screen to draw color
void Renderer::clear_screen() {
	SDL_RenderClear(ren);
}

//	Renderer::set_draw_color : set the current draw color for renderer
void Renderer::set_draw_color(Color c) {
	if (SDL_SetRenderDrawColor(ren, c.red(), c.green(), c.blue(), c.alpha()) != 0) {		// Set the draw color
		cerr<<"WARNING: "<<__func__<<": SDL_SetRenderDrawColor failed!\nSDL Error: "<<SDL_GetError()<<'\n';	// rcolor unchanged!
	}
	else {
		rcolor = c;		// update the rcolor value
	}
}

//	Renderer::reset_render_target : reset the render target
void Renderer::reset_render_target() {
	Renderer::set_render_target(nullptr);
}

//	Renderer::set_render_target : set the current render target to 'tex'
void Renderer::set_render_target(SDL_Texture* tex) {
	if (SDL_SetRenderTarget(ren, tex) != 0) {
		throw Exception(__func__ + string(": SDL_SetRenderTarget failed!\nSDL Error: ") + SDL_GetError());
	}
}

//////////////////////////////////////////////////////////////////////
//	class Texture
//////////////////////////////////////////////////////////////////////

//	Texture : sets the defaults
Texture::Texture() :
	tex(nullptr),
	tw(0),
	th(0)
{
}

//	~Texture : delete the Texture
Texture::~Texture() {
	free();
}

//	Texture::free : destroy the SDL_Texture
void Texture::free() {
	if (tex != nullptr) {
		SDL_DestroyTexture(tex);
		tw = 0;
		th = 0;
	}
}

//	Texture::create_blank : create a blank texture
void Texture::create_blank(int w, int h, SDL_TextureAccess access) {
	tex = SDL_CreateTexture(Renderer::renderer(), SDL_PIXELFORMAT_RGBA8888, access, w, h);	// create the texture
	if (tex == nullptr) {
		throw Bad_Texture(__func__ + string(": SDL_CreateTexture failed!\nSDL Error: ") + SDL_GetError());
	}
	else {
		tw = w;
		th = h;
	}
}

//	Texture::load_from_file : load the texture from 'path'
void Texture::load_from_file(string path, Color color_key) {
	free();		// free the current texture

	SDL_Texture* ntex = nullptr;	// the new texture
	SDL_Surface* surf = IMG_Load(path.c_str());	// get the SDL_Surface for the image
	if (surf == nullptr) {
		throw Bad_Texture(__func__ + string(": unable to load image: "+path+"\nIMG Error: ") + IMG_GetError());
	}
	
	SDL_SetColorKey(surf, color_key.alpha() == 0 ? SDL_FALSE : SDL_TRUE, SDL_MapRGB(surf->format, color_key.red(), color_key.green(), color_key.blue()));	// set the color key for surface
	ntex = SDL_CreateTextureFromSurface(Renderer::renderer(), surf);		//convert the surface to texture
	if (ntex == nullptr) {
		throw Bad_Texture(__func__ + string(": unable to convert surface to texture!\nSDL Error: ") + SDL_GetError());
	}
	// get the width and height of the texture
	tw = surf->w;
	th = surf->h;
	
	SDL_FreeSurface(surf);		// free the surface
	tex = ntex;	// set the new texture
}

//	Texture::set_blend_mode : set the blending mode of Texture
void Texture::set_blend_mode(SDL_BlendMode blend) {
	SDL_SetTextureBlendMode(tex, blend);
}

//	Texture::set_alpha : set the alpha of Texture (used in blending ops)
void Texture::set_alpha(Uint8 alpha) {
	SDL_SetTextureAlphaMod(tex, alpha);
}

//	Texture::render : render the texture
void Texture::render(const Point p, const Rect* clip, double angle, const Point* center, SDL_RendererFlip flip) {
	SDL_Rect ren_quad = { p.x, p.y, tw, th };		// the render quad of the texture (position in the current renderer's coordinates)
	SDL_Point* cen = nullptr;
	if (center != nullptr)
		cen = new SDL_Point { center->point().x, center->point().y };

	if (clip != nullptr) {
		ren_quad.w = clip->w;	// clip the Texture
		ren_quad.h = clip->h;
	}

	if (SDL_RenderCopyEx(Renderer::renderer(), tex, (SDL_Rect*)clip, &ren_quad, angle, cen, flip) != 0) {		//	clip provides source clipping while ren_quad provide destination clipping
		throw Bad_Texture (__func__ + string(": SDL_RenderCopyEx failed\nSDL error: ") + SDL_GetError());
	}
	if (center != nullptr)
		delete cen;
}

//	SDL::SDL : initialize the SDL system and set some hints
SDL::SDL (Uint32 flags) throw(SDL_Init_Error) {
	if (SDL_Init (flags) < 0) {		// initialize SDL system
		throw SDL_Init_Error (string("FATAL: Could not initialize SDL!\nSDL error: ") + SDL_GetError());
	}

	if (!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {		// set VSYNC
		cerr<<"WARNING: VSync not enabled!\n";
	}

	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {		// set linear texture filtering
		cerr<<"WARNING: Linear texture filtering not enabled!\n";
	}

	if (!(IMG_Init(IMG_FLAGS) & IMG_FLAGS)) {	// initialize SDL_Image subsystem
		cerr<<"FATAL: Could not initialize SDL_image!\nSDL_ error: "<<SDL_GetError()<<'\n';
	}
}

SDL::~SDL() {
    SDL_Quit();
}
