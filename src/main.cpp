#include "../include/base_game.h"
#include "../include/factory.h"
#include <iostream>

using namespace std;


int main() {
	int REN_HEIGHT = DEFAULT_HEIGHT;
	int REN_WIDTH = DEFAULT_WIDTH;
	const int LVL_WIDTH = 3600;
	const int LVL_HEIGHT = 3600;
	
	//REN_HEIGHT = LVL_HEIGHT; REN_WIDTH = LVL_WIDTH;
	const int CAMERA_WIDTH = REN_WIDTH;
	const int CAMERA_HEIGHT = REN_HEIGHT;

	try {
		SDL sdl;

		Window win("transistor");
		win.init();
	
		//win.pos(Point(0, 0));
		//win.resize(REN_WIDTH, REN_HEIGHT);
		//REN_HEIGHT = win.height();
		//REN_WIDTH = win.width();

		Renderer::create_renderer(win);
		Renderer::set_rendering_resolution (REN_WIDTH, REN_HEIGHT);

		Action_List animation_list;
		Action_List event_list;
		Control ctrl;
		Camera camera(Point(0, 0), CAMERA_WIDTH, CAMERA_HEIGHT);
		Event_Manager event_manager;
		Render_List render_list;
		Scene scene (Rect (Point (0,0), LVL_WIDTH, LVL_HEIGHT));

		
		Player_Factory player_factory;
		Player& player = player_factory.create_Player(Vec2(LVL_WIDTH/2, LVL_HEIGHT/2));

		event_manager.add_event_handler (win);
		event_manager.add_event_handler (ctrl);
		event_manager.add_state_handler (player);

		render_list.add_object (player);
		scene.add_dynamic_body (player.get_body());

		/*
		Texture tex[2];
		tex[0].load_from_file ("../rsc/ball_0_200.png", Color::WHITE);
		tex[1].load_from_file ("../rsc/ball_1_200.png", Color::WHITE);
		
		Texture_Stream ctex;
		ctex.add_texture (&tex[0]);
		ctex.add_texture (&tex[1]);

		const int ISIZE = 40;
		const int SSIZE = 80;
		const int SPEED = 10;
		const int SLICE = 20;
		const int ITEM_SIZE = 50;
		Sphere *item[ITEM_SIZE];
		for (int i = 0; i < ITEM_SIZE; ++i) {
			item[i] = new Sphere (&ctex, Vec2((i*ISIZE*8)%(LVL_WIDTH-SSIZE*10) + SSIZE*3, (i*ISIZE*8)/(LVL_WIDTH-SSIZE*10) * ISIZE*5 + SSIZE*20), Vec2(0, 0), 0.0f);
		}

		Guided_Sphere gs (&tex[0], Vec2 (ISIZE*20, ISIZE*8));
		gs.set_cmd ("xwdxxxxxxxxxxxxxxxxxxxxxxxxssxxxxxxxxxxxxxxxxxxxxxxxxaaxxxxxxxxxxxxxxxxxxxxxxxxwwxxxxxxxxxxxxxxxxxxxxxxxxsdxxxxxxxxxxxxxxxxxxxxxxxx");

		Rectangle_Texture* rtex1 = new Rectangle_Texture(LVL_WIDTH, SSIZE);
		Rectangle_Texture* rtex2 = new Rectangle_Texture(SSIZE, LVL_HEIGHT - 2 * SSIZE);
		Rectangle_Texture* rtex3 = new Rectangle_Texture(LVL_WIDTH / 2, SSIZE);
		Rectangle_Texture* rtex4 = new Rectangle_Texture(LVL_WIDTH / 4, SLICE);
		Rectangle_Texture* rtex5 = new Rectangle_Texture(LVL_WIDTH / 8, SLICE * 4);
		Rectangle_Texture* rtex6 = new Rectangle_Texture(SLICE * 4, LVL_WIDTH / 8);

		Moving_Slab* mslab1 = new Moving_Slab(rtex5, Vec2(SSIZE + SSIZE * 10, LVL_HEIGHT - SSIZE - SLICE * 14), Vec2 (0.0f, -20.0f), 1400.0f);
		Moving_Slab* mslab2 = new Moving_Slab(rtex6, Vec2(LVL_WIDTH - SSIZE * 3, LVL_HEIGHT / 2 - SSIZE * 5), Vec2 (-20.0f, 0.0), 1000.0f);
		Moving_Slab* mslab3 = new Moving_Slab(rtex5, Vec2(SSIZE * 3, LVL_HEIGHT / 2 - SSIZE * 4), Vec2 (20.0f, -20.0f), 1400.0f);
		Moving_Slab* mslab4 = new Moving_Slab(rtex5, Vec2(LVL_WIDTH - SSIZE * 20, LVL_HEIGHT / 2 + SSIZE), Vec2 (0.0f, -20.0f), 1000.0f);

		const int MOVING_SLAB_SIZE = 4;
		const int SLAB_SIZE = 12;
		Slab *slab[SLAB_SIZE+MOVING_SLAB_SIZE];
		slab[0] = new Slab(rtex1, Vec2(0, LVL_HEIGHT - SSIZE));
		slab[1] = new Slab(rtex1, Vec2(0, 0));
		slab[2] = new Slab(rtex2, Vec2(0, SSIZE));
		slab[3] = new Slab(rtex2, Vec2(LVL_WIDTH - SSIZE, SSIZE));
		slab[4] = new Slab(rtex3, Vec2(LVL_WIDTH / 2 - SSIZE, LVL_HEIGHT / 2 - SSIZE * 4));
		slab[5] = new Slab(rtex3, Vec2(SSIZE, LVL_HEIGHT / 2 - SSIZE * 8));
		slab[6] = new Slab(rtex4, Vec2(SSIZE, LVL_HEIGHT - SSIZE - SLICE * 6));
		slab[7] = new Slab(rtex4, Vec2(SSIZE + SLICE, LVL_HEIGHT - SSIZE - SLICE * 5));
		slab[8] = new Slab(rtex4, Vec2(SSIZE + SLICE * 2, LVL_HEIGHT - SSIZE - SLICE * 4));
		slab[9] = new Slab(rtex4, Vec2(SSIZE + SLICE * 3, LVL_HEIGHT - SSIZE - SLICE * 3));
		slab[10] = new Slab(rtex4, Vec2(SSIZE + SLICE * 4, LVL_HEIGHT - SSIZE - SLICE * 2));
		slab[11] = new Slab(rtex4, Vec2(SSIZE + SLICE * 5, LVL_HEIGHT - SSIZE - SLICE));
		slab[12] = mslab1;
		slab[13] = mslab2;
		slab[14] = mslab3;
		slab[15] = mslab4;


		//Circle_Texture* ptex = new Circle_Texture(ISIZE * 2, Color::GREEN);
		//Rectangle_Texture* ptex = new Rectangle_Texture(ISIZE * 2, ISIZE * 3);
		

		for (int i = 0; i < ITEM_SIZE; ++i) {
			scene.add_static_body(item[i]->get_body());
			alist.push_back (item[i]->get_action());
		}

		for (int i = 0; i < SLAB_SIZE; ++i) {
			scene.add_static_body(slab[i]->get_body());
			cerr<<"slab["<<i<<"]: ("<<slab[i]->min.x<<", "<<slab[i]->min.y<<"), ("<<slab[i]->max.x<<", "<<slab[i]->max.y<<")\n";
		}

		for (int i = SLAB_SIZE; i < MOVING_SLAB_SIZE+SLAB_SIZE; ++i) {
			scene.add_dynamic_body(slab[i]->get_body());
			cerr<<"slab["<<i<<"]: ("<<slab[i]->min.x<<", "<<slab[i]->min.y<<"), ("<<slab[i]->max.x<<", "<<slab[i]->max.y<<")\n";
		}



		scene.add_dynamic_body (p.get_body());
		scene.add_dynamic_body (gs.get_body());

		alist.push_back (mslab1->get_action());
		alist.push_back (mslab2->get_action());
		alist.push_back (mslab3->get_action());
		alist.push_back (mslab4->get_action());
		alist.push_back (p.get_action());

		elist.push_back (gs.get_action());
		

		Renderer::set_draw_color(Color::WHITE);

		Texture bg;
		bg.load_from_file ("../rsc/bg.png");

		cerr<<"QUADTREE HEIGHT: "<<scene.get_height()<<'\n';
		cerr<<"QUADTREE: \n";
		scene.draw_static_tree();
		*/

		const float fps = 40.0;
		const float dt = 1/fps;
		const float MAX_ACC = 0.2f;

		float accumulator = 0;
		float curr_fps;

		unsigned int frames = 0;
		unsigned int timer = 0;
		unsigned int t1 = 0;
		unsigned int start_time = SDL_GetTicks();

		unsigned int frame_start = SDL_GetTicks();

		while (!win.quit()) {
			timer = SDL_GetTicks();
			
			event_manager.poll_handle();

			if (win.is_hidden() || ctrl.is_paused())
				continue;

			event_list.update (dt);

#ifdef DEBUG
			cerr<<"accumulator: "<<accumulator<<'\n';
			t1 = SDL_GetTicks();
#endif

			accumulator += timer - frame_start;
			frame_start = timer;

			if (accumulator > MAX_ACC)
				accumulator = MAX_ACC;

			while (accumulator > dt) {
				scene.step (dt);
				accumulator -= dt;
			}

			animation_list.update (dt);

	
#ifdef DEBUG
			cerr<<"timer: "<<SDL_GetTicks() - t1<<'\n';
			cerr<<"accumulator: "<<accumulator<<'\n';
#endif

			// render
			camera.update_position (player.get_body()->pos, LVL_WIDTH, LVL_HEIGHT);
			render_list.render_all(camera);

			/*
			bg.render(Point(0,0), &camera);
			for (int i = 0; i < ITEM_SIZE; ++i)
				item[i]->render(camera);
			for (int i = 0; i < SLAB_SIZE+MOVING_SLAB_SIZE; ++i)
				slab[i]->render(camera);
			p.render(camera);
			gs.render(camera);
			//...
			*/


#ifdef DEBUG
			cerr<<"player_velocity: "<<p.get_body()->velocity.x<<' '<<p.get_body()->velocity.y<<'\n';
#endif

			timer = SDL_GetTicks() - timer;
			++frames;

#ifdef DEBUG
			curr_fps = 1000.0f/float(timer);
			cerr<<"fps: "<<curr_fps<<'\n';	// print current fps
#endif

		}
		timer = SDL_GetTicks()-start_time;
		cout<<"Frames : "<<frames<<"\nTime Taken : "<<timer/1000.0f<<"\nFPS : "<<frames/(timer/1000.0f)<<'\n';
	}
	catch (Exception& e) {
		cerr<<"Exception : "<<e.what()<<'\n';;
	}
	// deallocate memory?

	return 0;
}
