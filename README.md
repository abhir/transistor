transistor
==========

A 2D platformer game

How to Build
============

Linux
-----

You should have SDL2 installed in your system. If not, get it (and build) from [SDL website](https://www.libsdl.org) or for Debian-based systems: `apt-get install libsdl2*`.

* `git clone https://github.com/rawcoder/transistor.git` or [Download Zip](https://github.com/rawcoder/transistor/archive/master.zip).
* `cd transistor/src && make`

