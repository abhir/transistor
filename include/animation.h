#include "../include/common.h"
#include "../include/graphics.h"

/*	Texture_Stream : streams textures for generating animations	*/
/*	NOTE: not responsible for allocating and deallocating the memory for Texture(s)	*/
class Texture_Stream {
public:
	Texture_Stream()
	{	}
	
	//	add the texture to the list
	void add_texture (Texture* t) {
		stream.push_back (t);
	}

	//	return the Texture at position i mod (number_of_textures)
	Texture* get_texture (int i) const {
		return stream[i%(stream.size())];
	}
private:
	vector <Texture*> stream;
};
