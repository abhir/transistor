#ifndef TRANS_TILE_MAP_HEAD
#define TRANS_TILE_MAP_HEAD

#include "gamedata.h"
#include "common.h"

const int OBJECT_PER_TILE = 5;
const int TILE_SIZE = 40;
const int TILE_MAP_WIDTH = 2200;
const int TILE_MAP_HEIGHT = 1800;
const int TILE_COUNT_X = TILE_MAP_WIDTH / TILE_SIZE;
const int TILE_COUNT_Y = TILE_MAP_HEIGHT / TILE_SIZE;

struct Tile_Map {
	typedef array<Object*, TILE_TYPE::OBJECT_PER_TILE> Objects;
	Objects tiles[TILE_COUNT_X][TILE_COUNT_Y];

	Tile_Map () {
		for (int i = 0; i < TILE_COUNT_X; ++i)
			for (int j = 0; j < TILE_COUNT_Y; ++j)
				for (int k = 0; k < OBJECT_PER_TILE; ++k)
					tiles[i][j][k] = nullptr;
	}

	void update_tile (Object* obj, const Point& pos) {
		if (tiles.size() == tiles.max_size()) {
			cerr<<"ERROR: Tile_Map::"<<__func__<<": trying to overflow dynamic tile["<<pos.x<<"]["<<pos.y<<"]\n";
			return;
		}
		tiles[pos.x][pos.y][tiles[pos.x][pos.y].size()] = obj;
	}

	void add_static_tile (Object* obj, const Point& pos) {
		if (tiles[pos.x][pos.y][STATIC_TILE] != nullptr)
			cerr<<"ERROR: Tile_Map::"<<__func__<<": trying to overflow static tile["<<pos.x<<"]["<<pos.y<<"]\n";
			return;
		}
		tiles[pos.x][pos.y][i] = obj;
	}



};

#endif
