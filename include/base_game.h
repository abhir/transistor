#ifndef TRANS_BASE_GAME_HEAD
#define TRANS_BASE_GAME_HEAD

#include "common.h"
#include "graphics.h"
#include "physics.h"
#include "action.h"
#include "animation.h"
#include "event.h"

struct Game_Object {
	virtual Body* get_body () = 0;
	virtual Action* get_action () = 0;
	virtual Shape* get_shape () = 0;
	virtual void render (const Camera&) = 0;
};

struct Render_List {
	Render_List () {}

	void add_object (Game_Object& obj) {
		// TODO: check for multiple additions
		objects.push_back (&obj);
	}

	void render_all (const Camera& camera) {
		Renderer::set_draw_color (Color::WHITE);
		Renderer::clear_screen ();
		for (auto& i : objects) {
			i->render (camera);
		}
		Renderer::render_screen ();
	}

private:
	vector <Game_Object*> objects;
};

class Circle_Texture : public Texture {
public:
	Circle_Texture (unsigned radius, Color color = Color::BLUE);

private:
	Circle c;
};

class Rectangle_Texture : public Texture {
public:
	Rectangle_Texture(int w, int h);
};

struct Sphere : public Action, public Game_Object {
	Texture_Stream* tex_stream;
	Body body;
	Bounding_Circle shape;
	unsigned curr_frame;

	Sphere (Texture_Stream* texture_stream, Vec2 top_left_position, Vec2 velocity = Vec2(), float inv_mass = 1.0f);

	void update (float dt);

	void render(const Camera& camera);

	virtual Body* get_body () { return &body; }
	virtual Action* get_action () { return this; }
	virtual Shape* get_shape () { return &shape; }
};

struct Guided_Sphere : public Bounding_Circle, public Action {
	Texture* tex;
	Body gs_body;
	string cmd;
	int curr_cmd;

	Guided_Sphere (Texture* texture, Vec2 top_left_position, Vec2 velocity = Vec2(), float inv_mass = 0.1f);

	void set_cmd (const string& c) {
		cmd = c;
	}

	void on_start ();
	
	void update (float dt);

	void render (const Camera& camera);

	virtual Body* get_body () { return &gs_body; }
	virtual Action* get_action () { return this; }
	virtual Shape* get_shape () { return this; }
};

struct Slab : public Bounding_Box, public Game_Object {
	Rectangle_Texture* rtex;
	Body slab_body;

	Slab (Rectangle_Texture* texture, Vec2 top_left_position, Vec2 velocity = Vec2(), float inv_mass = 0.0f);

	void render(const Camera&);

	virtual void post_collision (const Manifold&)
	{	}

	virtual Body* get_body () { return &slab_body; }
	virtual Action* get_action () { return nullptr; }
	virtual Shape* get_shape () { return this; }
};

struct Player_Box : public Bounding_Box {
	Texture_Stream* ptex;
	unsigned curr_frame;
	Body player_body;

	Player_Box (Texture_Stream* texture_stream, Vec2 top_left_position, Vec2 velocity = Vec2(), float inv_mass = 0.0f);

	void render(const Camera& camera);

	virtual void pre_collision (const Manifold&)
	{	}
	virtual void post_collision (const Manifold&)
	{	}

	virtual ~Player_Box()
	{	}
};


struct Player : public Player_Box, public Event_Handler, public Action, public Game_Object {
public:
	unsigned JUMP_KEY, JUMP_BOOST_KEY, TURN_LEFT_KEY, TURN_RIGHT_KEY;

	Player (Texture_Stream* texture_stream, Vec2 top_left_position, Vec2 velociy = Vec2(), float inv_mass = 1.0f);
		
	void update (float dt);

	void pre_collision (const Manifold& m);

	void post_collision (const Manifold& m);

	void handle_state (const Uint8* key_state);

	Body* get_body () { return &player_body; }
	Action* get_action () { return this; }
	Shape* get_shape () { return this; }
	void render (const Camera& cam) { Player_Box::render(cam); }

private:
	float friction1, friction2;
	bool can_jump;
};

class Moving_Slab : public Action, public Slab {
public:
	Moving_Slab (Rectangle_Texture* texture, Vec2 top_left_position, Vec2 velocity = Vec2(), float slab_timer = 50.0f, float inv_mass = 0.0f) :
		Slab (texture, top_left_position, velocity, inv_mass),
		TIMER (slab_timer),
		VEL1 (velocity),
		tick (0.0f)
	{	
	}

	void on_start () {
		slab_body.velocity = VEL1;
		tick = TIMER;
	}

	void update (float dt) {
		tick -= (slab_body.velocity * dt).magnitude();
		if (tick <= 0.0f) {
			slab_body.velocity = -slab_body.velocity;
			tick = TIMER;
		}
	}

	void on_end () {
		slab_body.velocity.set(0.0f, 0.0f);
	}

	virtual Action* get_action () { return this; }

private:
	float TIMER;
	Vec2 VEL1;
	float tick;
};

class Control : public Event_Handler {
public:
	Control () : Event_Handler (SDL_KEYDOWN | SDL_KEYUP), paused (false)
	{	}

	void handle_event (const SDL_Event& e) {
		if (e.type == SDL_KEYDOWN) {
			if (e.key.keysym.sym == SDLK_p)
				paused = !paused;
		}
	}

	bool is_paused () const {
		return paused;
	}

private:
	bool paused;
};

#endif
