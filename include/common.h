#ifndef TRANS_COMMON_HEAD
#define TRANS_COMMON_HEAD

#include <SDL.h>
#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <map>
#include <new>		// for bad_alloc
using namespace std;

#define CLAMP(a, b, x) min(max((a), (x)), (b))

typedef unsigned long ID;
/*	ID_gen : Unique ID generator for game objects	*/
struct ID_gen {
	static ID next_ID;
	static ID get_ID () {
		return next_ID++;
	}
};

/*	Vec2 : a 2-Vector (two dimensions)	*/
struct Vec2 {
	float x, y;

	Vec2() : x(0.0f), y(0.0f) {	}
	Vec2(float i, float j) : x(i), y(j) {	}

	float magnitude() const {
		return sqrt(x*x + y*y);
	}

	void normalize() {
		if (x == 0.0f && y == 0.0f)
			return;
		float mag = magnitude();
		set(x/mag, y/mag);
	}

	void set(float i, float j) {
		x = i;
		y = j;
	}
};

Vec2 operator - (const Vec2& a, const Vec2& b);
Vec2 operator - (const Vec2& a, float c);
Vec2 operator - (const Vec2& a);
Vec2 operator -= (Vec2& a, const Vec2& b);

Vec2 operator + (const Vec2& a, const Vec2& b);
Vec2 operator + (const Vec2& a, float c);
Vec2 operator += (Vec2& a, const Vec2& b);

Vec2 operator * (const Vec2& a, float f);
Vec2 operator / (const Vec2& a, float f);

bool operator == (const Vec2& a, const Vec2& b);
bool operator < (const Vec2& a, const Vec2& b);

float dot_product (const Vec2& a, const Vec2& b);
Vec2 abs(const Vec2& a);

/*	Point : wrapper for SDL_Point	*/
struct Point {
	Point () : x(0), y(0) {	}
	Point (int xx, int yy) : x(xx), y(yy) {	}
	Point (Vec2 v) : x(v.x), y(v.y) { }

	SDL_Point point() const {
		SDL_Point p = { x, y };
		return p;
	}

	int x;
	int y;
};

/*	Rect : wrapper for SDL_Rect	*/
struct Rect {
	Rect(): x(0), y(0), w(0), h(0) {	}
	Rect (int xx, int yy, int ww, int hh) :
		x(xx), y(yy), w(ww), h(hh)
	{	}
	Rect (Point p, int ww, int hh) :
		x(p.x), y(p.y), w(ww), h(hh)
	{	}
	Rect (Point min, Point max) :
		x(min.x), y(min.y), w(max.x-min.x), h(max.y-min.y)
	{	}

	SDL_Rect rect() const {
		SDL_Rect r = { x, y, w, h };
		return r;
	}

	int x, y;
	int w, h;
};


/*	Circle : represents a Circle (view part)	*/
struct Circle {
	vector<Point> points;

	Circle() {	}

	void calculate_points(const Point& centre, unsigned int radius);	// calculate points required for rendering using Mid point circle algorithm

	// pointer to array of points
	Point* data() {
		return points.data();
	}

	unsigned size() const {
		return points.size();
	}
};

/*	Exception : Base class of all the exceptions	*/
class Exception {
public:
	Exception() : msg("Unknown Exception") {	}
	Exception(string str) : msg(str) {	}

	virtual string what() const {		// called after 'catch'
		return msg;
	}

protected:
	string msg;	// the message displayed by what()
};

#endif
