#ifndef TRANS_EVENT_HEAD
#define TRANS_EVENT_HEAD

#include "common.h"
/*	Event_Handler : All event-handlers must subclass it to receive events from the Event_Manager	*/
class Event_Handler {
public:
	Event_Handler() : events(0)				{			}
	Event_Handler(Uint32 e) : events(e)			{			}

	// handle the event passed (NOTE: keep the function small as it is called for every event received by Event_Manager)
	virtual void handle_event(const SDL_Event&)		{			}
	virtual void handle_state(const Uint8* key_state)				{			}

	// returns the event type the instance is interested in	
	Uint32 event_type() const			{	return events;	}	
protected:
	Uint32 events;
};


/*	Event_Manager : wrapper for SDL_Event. Combined with 'Event_Handler' provides a neat way of handling events	*/
class Event_Manager {
public:
	Event_Manager();
	~Event_Manager();

	// add a handler of events and states
	void add_state_handler(Event_Handler&);
	void add_event_handler(Event_Handler&);

	// remove an added handler
	void remove_state_handler(Event_Handler&);
	void remove_event_handler(Event_Handler&);

	// poll for events and call the handlers for handling them
	void poll_handle();
private:
	SDL_Event event;
	Uint8* keyboard_state;	// saves the current state of keyboard (useful for state handlers)
	vector <Event_Handler*> state_handlers;
	vector <Event_Handler*> event_handlers;
};

#endif
