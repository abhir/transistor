#ifndef TRANS_ACTION_HEADER
#define TRANS_ACTION_HEADER

#include "common.h"

class Action_List;

/*	Action : represent an Action which can be executed by an Action_List	*/
class Action {
public:
	Action () :
		is_finished (false),
		is_blocking (false)
	{	}

	virtual void update (float dt)	{	}	// called by Action_List for each update
	virtual void on_start ()	{	}	// executed when Action is added to Action_List
	virtual void on_end ()		{	}	// executed when Action is removed from Action_List (is_finished becomes true)

	bool is_finished;	// true if Action has finished its job
	bool is_blocking;	// true if Action blocks low priority Actions in the Action_List
	//unsigned lanes;
	//float elapsed;
	//float duration;
	Action_List *owner_list;	// the Action_List to which this Action belongs to
};

/*	Action_List : models a finite state machine for a list of Action(s)	*/
class Action_List {
	typedef vector <Action*>::iterator Actions_iterator;
public:
	Action_List () : iter(actions.begin())
	{	}

	void update (float dt);		// calls the update() method for each Action

	// add Actions to the list at the start and end
  	void push_front (Action *action);
	void push_back (Action *action);

	// insert Action before and after a certain Action_iterator (points to Actions in the list)
	void insert_before (const Actions_iterator& iter, Action *action);
	void insert_after (Actions_iterator iter, Action *action);

	// remove Action from the list
	void remove (Actions_iterator iter);

	Action *begin () 	{	return *(actions.begin());	}	// return the first Action in the list
	Action *end ()		{	return *(actions.end());	}	// return the last Action in the list
	bool is_empty () const	{	return actions.size() == 0;	}	// returns true if the list is empty

	Actions_iterator& current_iterator()	{	return iter;	}	// returns the iterator for currently executing Action (useful for an Action, for appending Actions after itself)
private:
	//unsigned lanes;
	vector <Action*> actions;
	Actions_iterator iter;
};

#endif
