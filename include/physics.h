#ifndef TRANS_PHYSICS_HEAD
#define TRANS_PHYSICS_HEAD

#include "common.h"

struct Shape;
//	some useful defaults
const float GRAVITY_DEFAULT = 1.0f;
const float MASS_EPSILON = 1.0e-10;
const float VELOCITY_EPSILON = 0.5f;

const Vec2 GRAVITY_ACCELERATION = Vec2(0, 10.0f);
const Vec2 FRICTION_EPSILON = Vec2(1.0e-15, 1.0e-15);
const Vec2 IMPULSE_EPSILON = Vec2(1.0e-15, 1.0e-15);

struct Manifold;
/*	Body : models a physical body	*/
struct Body {
	Body () : id (ID_gen::get_ID ()) {	}

	ID id;	// used for identification purposes

	Shape *shape;	// the Shape which the Body represents
	float inv_mass;
	float restitution;

	float static_friction;
	float dynamic_friction;
	
	Vec2 pos;
	Vec2 velocity;
	Vec2 force;
	float gravity_scale;	// gravity is calculated as : gravity_scale * GRAVITY_DEFAULT

	//	integrate the velocity and forces for time dt
	void integrate_forces (float dt);
	void integrate_velocity (float dt);
};

/*	Shape : models the shape of a Body	*/
struct Shape {
	enum Type { BOX, CIRCLE, SIZE };

	Type type;
	unsigned layer;		// layer to which the Body belongs to
	Body* body;		// not OWNED by Shape
	bool active;	// active body gets resolved after a collision

	Shape() :
		layer (1),	// layer should NEVER be 0
		body (nullptr),
		active (true)
	{	}
	virtual void move () = 0;
	virtual void pre_collision (const Manifold& m)		// called after a collision has been detected
	{	}
	virtual void post_collision (const Manifold& m)		// called after a collsion has been resolved
	{	}

	virtual Rect get_bounding_box () const = 0;		// the bounding box for the shape, useful for composite bodies

	virtual ~Shape()
	{	}
};

/*	Bounding_Circle : a circular shaped body	*/
struct Bounding_Circle : public Shape {
	Vec2 centre;	// centre of circle
	float rad;	// radius

	Bounding_Circle () {
		type = Shape::CIRCLE;
 		rad = 0.0f;
	}

	Bounding_Circle (Vec2 centre_point, float radius) {
		type = Shape::CIRCLE;
		centre = centre_point;
		rad = radius;
	}

	virtual void move () {
		centre.set(body->pos.x, body->pos.y);
	}
	virtual void pre_collision (const Manifold&) { } 
	virtual void post_collision (const Manifold&) { }
	virtual Rect get_bounding_box () const {
		return Rect (Point(centre-rad), 2*rad, 2*rad);
	}

	virtual ~Bounding_Circle()
	{	}
};

/*	Bounding_Box : an Axis Aligned Bounding Box	*/
struct Bounding_Box: public Shape {
	Vec2 min, max;	// min represents top-left corner while max represents bottom right one

	Bounding_Box () {
		type = Shape::BOX;
	}
	Bounding_Box (Vec2 top_left, Vec2 bottom_right) {
		type = Shape::BOX;
		min = top_left;
		max = bottom_right;
	}

	virtual void move () {	// adjusts the min, max according to Body position
		const float ex = (max.x-min.x)/2;
		const float ey = (max.y-min.y)/2;

		min.set(body->pos.x-ex, body->pos.y-ey);
		max.set(body->pos.x+ex, body->pos.y+ey);
	}

	virtual void pre_collision (const Manifold&) {	}
	virtual void post_collision (const Manifold&) {	}
	virtual Rect get_bounding_box () const {
		return Rect (Point(min.x, min.y), Point(max.x, max.y));
	}

	virtual ~Bounding_Box()
	{	}
};

/*	Manifold : represents a collision test pair	*/
struct Manifold {
	Body* a;
	Body* b;
	float penetration;	// the depth by which the two Body(s) have penetrated into each other
	Vec2 normal;		// normal of collision

	Manifold () : a (nullptr), b (nullptr), penetration (0.0f), normal () {	}
	Manifold (Body* body1, Body* body2): a (body1), b (body2), penetration (0.0f), normal () {	}

	void resolve_collision ();	// resolve the collision
	void positional_correction ();	// correct positions to avoid sinking effect
};

/*	Scene : models a scene of physics engine	*/
/*	Bodies move and collide with each other and are resloved by the engine	*/
struct Scene {
	/*	Quadtree : for broadphase collision test	*/
	class Quadtree {
		private:
			static const int MAX_LEVELS = 20;
			static const int MAX_OBJECTS_PER_LEVEL = 10;

			// each node is bounded by "bounds" and contain "objects"
			int level;
			vector <Body*> objects;
			Rect bounds;
			Quadtree *nodes[4];

		public:
			Quadtree (int lvl, const Rect& qbounds)
				: level(lvl)
			{
				bounds = qbounds;
				nodes[0] = nullptr;
				nodes[1] = nullptr;
				nodes[2] = nullptr;
				nodes[3] = nullptr;
			}

			void clear ();		// *recursively* delete the current node i.e. the child nodes also get clear()'d
			bool split ();
			int get_index (Rect);	// return the index of node (region) to which the "Rect" belongs to
			void insert (Rect, Body*);
			void do_broadphase (Body*, Scene*);

			// @TODO: remove in release build, DEBUG only!
			int height (const Quadtree* q) const {
				if (q == nullptr)
					return 0;
				return std::max (std::max (height (q->nodes[0]), height (q->nodes[1])), std::max(height (q->nodes[2]), height(q->nodes[3]))) + 1;
			}
			void draw_tree (const Quadtree* q, const string spacing = "") const {
				if (q == nullptr)
					return;

				cerr<<spacing<<" ("<<q->bounds.x<<", "<<q->bounds.y<<" +"<<q->bounds.w<<" +"<<q->bounds.h<<") : "<<q->objects.size()<<'\n';
				draw_tree (q->nodes[0], spacing+"\t");
				draw_tree (q->nodes[1], spacing+"\t");
				draw_tree (q->nodes[2], spacing+"\t");
				draw_tree (q->nodes[3], spacing+"\t");
			}
	};

	Scene (const Rect& dims)
		: static_bodies (0, dims)
	{	}

	// add Body to scene
	void add_static_body (Body* b) {
		Rect r = b->shape->get_bounding_box();
		cerr<<__func__<<": ("<<r.x<<", "<<r.y<<" + "<<r.w<<" + "<<r.h<<'\n';
		static_bodies.insert(b->shape->get_bounding_box(), b);
	}
	
	void add_dynamic_body (Body* b) {
		dynamic_bodies.push_back (b);
	}

	// remove a Body from the scene
	void remove_dynamic_body (Body* b) {
		for (Body_iterator i = dynamic_bodies.begin(); i != dynamic_bodies.end(); ++i)
			if (*i == b) {
				dynamic_bodies.erase (i);
				break;
			}
	}
	
	// step the whole scene by dt time
	void step (float dt);

	void check_resolve_collision (Body*, Body*);

	// @TODO : DEBUG only
	int get_height() const {
		return static_bodies.height(&static_bodies);
	}

	void draw_static_tree () const {
		static_bodies.draw_tree (&static_bodies);
	}

private:
	vector <Body*> dynamic_bodies;
	Quadtree static_bodies;
	typedef vector <Body*>::iterator Body_iterator;
};

#endif
