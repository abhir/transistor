#ifndef TRANS_FACTORY_HEAD
#define TRANS_FACTORY_HEAD

#include "base_game.h"

struct Player_Factory {
	static const int PLAYER_TEX_SIZE = 8;
	Texture player_tex[PLAYER_TEX_SIZE];
	Texture_Stream player_tex_stream;
	vector<Player*> objects;

	Player& create_Player (Vec2 position) {
		static bool create_tex = true;

		if (create_tex) {
			player_tex[0].load_from_file ("../rsc/new_x_0.png", Color::WHITE);
			player_tex[1].load_from_file ("../rsc/new_x_1.png", Color::WHITE);
			player_tex[2].load_from_file ("../rsc/new_x_2.png", Color::WHITE);
			player_tex[3].load_from_file ("../rsc/new_x_3.png", Color::WHITE);
			player_tex[4].load_from_file ("../rsc/new_x_4.png", Color::WHITE);
			player_tex[5].load_from_file ("../rsc/new_x_5.png", Color::WHITE);
			player_tex[6].load_from_file ("../rsc/new_x_6.png", Color::WHITE);
			player_tex[7].load_from_file ("../rsc/new_x_7.png", Color::WHITE);

			for (int i = 0; i < PLAYER_TEX_SIZE; ++i)
				player_tex_stream.add_texture (&player_tex[i]);
		}

		objects.push_back(new Player(&player_tex_stream, position));
		create_tex = false;

		return *objects.back();
	}

	~Player_Factory() {
		for (auto& i : objects) {
			delete i;
		}
	}
};

#endif
